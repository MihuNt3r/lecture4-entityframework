﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Lecture4.Models.AuxiliaryModels;
using Lecture4.Models.DTOs;
using Lecture4.Services;

namespace Lecture4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _service;

        public LinqController(LinqService service)
        {
            _service = service;
        }

        [HttpGet("getdictionary{id}")]
        public ActionResult<Dictionary<ProjectDTO, int>> GetDictionary(int id)
        {
            return _service.GetDictionary(id);
        }

        [HttpGet("gettasksforuser/{id}")]
        public ActionResult<List<TaskDTO>> GetListOfTasks(int id)
        {
            return _service.GetTasksForUserById(id);
        }

        [HttpGet("gettasksfinishedin2021byuser/{id}")]
        public ActionResult<List<TaskIdAndName>> GetFinishedTasks(int id)
        {
            return _service.GetTasksFinishedIn2021ByUser(id);
        }

        [HttpGet("getteamswhereallusersolderthan10")]
        public ActionResult<List<TeamIdNameAndUsers>> GetTeamsWithUsersOlderThan10()
        {
            return _service.GetListOfTasksWithUsersOlderThan10();
        }

        [HttpGet("getsorteduserswithsortedtasks")]
        public ActionResult<List<UserNameAndSortedTasks>> GetSortedUsersAndSortedTasks()
        {
            return _service.GetSortedUsersWithSortedTasks();
        }

        [HttpGet("getuserandinfoabouthistasksandprojects/{id}")]
        public ActionResult<UserAndInfoAboutHisTasksAndProjects> GetUserAndInfoAboutHisTasksAndProjects(int id)
        {
            return _service.GetUserAndInfoAboutHisTasksAndProjects(id);
        }

        [HttpGet("getprojectandinfoaboutitstasksandcustomers")]
        public ActionResult<List<ProjectAndInfoAboutItsTasksAndCustomers>> GetProjectsInfo()
        {
            return _service.GetProjectAndInfoAboutItsTasksAndCustomers();
        }
    }
}
