﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Lecture4.Models.Models;

namespace Lecture4.DataAccess
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<User>()
                .Property(u => u.FirstName)
                .IsRequired()
                .HasMaxLength(25);

            modelBuilder
                .Entity<User>()
                .Property(u => u.LastName)
                .IsRequired()
                .HasMaxLength(25);

            modelBuilder
                .Entity<Task>()
                .Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder
                .Entity<Project>()
                .Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50);





            var teams = new List<Team>
            {
                new Team { Id = 1, Name = "Denesik - Greenfelder", CreatedAt = DateTime.Parse("25.08.2019") },
                new Team { Id = 2, Name = "Durgan Group", CreatedAt = DateTime.Parse("31.03.2017")},
                new Team { Id = 3, Name = "Kassulke LLC", CreatedAt = DateTime.Parse("21.02.2019")},
                new Team { Id = 4, Name = "Harris LLC", CreatedAt = DateTime.Parse("28.08.2018")}
            };

            var users = new List<User>
            {
                new User { Id = 1, BirthDay = DateTime.Parse("23.11.1953"), Email = "Vivian99@yahoo.com", FirstName = "Vivian", LastName = "Mertz", RegisteredAt = DateTime.Parse("19.10.2018"), TeamId = 4},
                new User { Id = 2, BirthDay = DateTime.Parse("23.07.1973"), Email = "Felizia.Kirilin@yahoo.com", FirstName = "Felicia", LastName = "Kirilin", RegisteredAt = DateTime.Parse("20.10.2019"), TeamId = 2},
                new User { Id = 3, BirthDay = DateTime.Parse("14.02.1977"), Email = "Jared_Anderson@yahoo.com", FirstName = "Jared", LastName = "Anderson", RegisteredAt = DateTime.Parse("20.10.2020"), TeamId = 2}
            };

            var projects = new List<Project>
            {
                new Project { Id = 1, Name = "Dem", AuthorId = 2, Deadline = DateTime.Parse("21.07.2021"), Description = "Quia et tempora hic pariatur voluptatem doloribus sunt.", TeamId = 4}
            };

            var tasks = new List<Task>
            {
                new Task { Id = 1, CreatedAt = DateTime.Parse("23.11.2018"), Description = "Delectus quibusdam id quia iure neque maiores molestias sed aut", FinishedAt = null, Name = "withdrawal contextually-based", PerformerId = 3, ProjectId = 1, State = 2},
                new Task { Id = 2, CreatedAt = DateTime.Parse("23.11.2018"), Description = "Lorem Ipsum", FinishedAt = DateTime.Parse("23.11.2021"), Name = "Dolor sit", PerformerId = 2, ProjectId = 1, State = 2}
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);

            base.OnModelCreating(modelBuilder);
        }
    }
}
